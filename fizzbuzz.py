import sys
print "0.4"
print "This is the name of the script: ", sys.argv[0]
#print "This is the max number: ", sys.argv[1]
print "Number of arguments: ", len(sys.argv)
print "The arguments are: " , str(sys.argv)
if len(sys.argv) < 2:
   print "No command line argument is passed"
   print "Max set to 15 as default, so it will list fizzbuzz from 1 to 15"
   print ""
   max = 15
else:
   max=int(sys.argv[1])

for fizzbuzz in range(1,max+1):
    if fizzbuzz % 3 == 0 and fizzbuzz % 5 == 0:
        print("fizzbuzz")
        continue
    elif fizzbuzz % 3 == 0:
        print("fizz")
        continue
    elif fizzbuzz % 5 == 0:
        print("buzz")
        continue
    print(fizzbuzz)
	

print ""

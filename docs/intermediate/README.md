
## Objective:
- Using your favorite orchestration (i.e. Kubernetes) tools and platform, provide a working infrastructure-as-code example that provisions a service to execute an implementation of the FizzBuzz coding challenge with documentation on how to use and execute.
- Test private docker repository (hosted @ [docker hub](https://cloud.docker.com/repository/docker/amitkarpe/fbprivate)) using 

### Prerequisite:

- Working cluster with Master & Node
- Node should able to run all kubectl related command. ( copy /etc/kubernetes/admin.conf from Master to ~/.kube/config on Node1)

```console
cloud_user@node1 $ kubectl get nodes
NAME                          STATUS   ROLES    AGE     VERSION
amitkarpe2c.mylabserver.com   Ready    master   6h35m   v1.13.4
amitkarpe3c.mylabserver.com   Ready    <none>   6h29m   v1.13.4
```

### Steps to run the code

```shell
git clone https://gitlab.com/amitkarpe/fizzbuzz.git
cd fizzbuzz/intermediate/
make
```

### Troubleshooting


- Make sure node1 have `fizzbuzz` docker image 
```console
cloud_user@amitkarpe3c:~/fizzbuzz/intermediate$  docker images | grep fbprivate
amitkarpe/fbprivate      0.1                 0467633ad7ab        17 minutes ago      138MB
```

- Make sure user have permission to create k8s objects like pod.
- Review `kubectl describe job fbprivate` & `kubectl describe pod fbprivate`

```console
cloud_user@amitkarpe3c:~/$  kubectl describe pod fbprivate
Name:           fbprivate-jxb8f
Namespace:      default
Priority:       0
Node:           amitkarpe3c.mylabserver.com/172.31.24.57
Start Time:     Mon, 07 Oct 2019 10:22:21 +0000
Labels:         controller-uid=5943cf87-e8ec-11e9-868b-0694f6454bca
                job-name=fbprivate
                run=fbprivate
Annotations:    <none>
Status:         Pending
IP:             10.244.1.55
Controlled By:  Job/fbprivate
Containers:
  fbprivate:
    Container ID:   
    Image:          amitkarpe/fbprivate:0.1
    Image ID:       
    Port:           <none>
    Host Port:      <none>
    State:          Waiting
      Reason:       ImagePullBackOff
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8mksx (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  default-token-8mksx:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-8mksx
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason     Age                     From                                  Message
  ----     ------     ----                    ----                                  -------
  Normal   Scheduled  9m51s                   default-scheduler                     Successfully assigned default/fbprivate-jxb8f to amitkarpe3c.mylabserver.com
  Normal   Pulling    8m20s (x4 over 9m52s)   kubelet, amitkarpe3c.mylabserver.com  pulling image "amitkarpe/fbprivate:0.1"
  Warning  Failed     8m17s (x4 over 9m49s)   kubelet, amitkarpe3c.mylabserver.com  Failed to pull image "amitkarpe/fbprivate:0.1": rpc error: code = Unknown desc = Error response from daemon: pull access denied for amitkarpe/fbprivate, repository does not exist or may require 'docker login'
  Warning  Failed     8m17s (x4 over 9m49s)   kubelet, amitkarpe3c.mylabserver.com  Error: ErrImagePull
  Normal   BackOff    7m50s (x6 over 9m48s)   kubelet, amitkarpe3c.mylabserver.com  Back-off pulling image "amitkarpe/fbprivate:0.1"
  Warning  Failed     4m41s (x19 over 9m48s)  kubelet, amitkarpe3c.mylabserver.com  Error: ImagePullBackOff
```

- Delete all pod which have label as `run=fbprivate` using command `kubectl delete all -l run=fizzbuzz`
- Create pod using following command `kubectl run fbprivate --image=fbprivate --image-pull-policy=OnFailure --restart=Never`
- If any error related to Image Pull occured, then make sure that the `imagePullPolicy` of the container to is set to `Never`. Ref: [Updating Images](https://kubernetes.io/docs/concepts/containers/images/#updating-images)
- If still image pull failed then make sure that on the same system `docker images` showing specified image.
- If this is "Error: ErrImagePull" then make sure that K8S have credential to access this image.

```console
cloud_user@node1 $ cat $HOME/.docker/config.json             
{
	"auths": {
		"https://index.docker.io/v1/": {
			"auth": "YW1pdGthcnBlOjAxYTUyMjI0LTRhYjEtNDczYy04MzljLWM5YTQ1NjEzOTAzZQ=="
		}
	},
	"HttpHeaders": {
		"User-Agent": "Docker-Client/18.09.7 (linux)"
	}
}%                                                           
```
- Creating imagePullSecrets by `kubectl create secret docker-registry`

```
Examples:
  # If you don't already have a .dockercfg file, you can create a dockercfg secret directly by using:
  kubectl create secret docker-registry my-secret --docker-server=DOCKER_REGISTRY_SERVER --docker-username=DOCKER_USER --docker-password=DOCKER_PASSWORD --docker-email=DOCKER_EMAIL
  OR
  kubectl create secret generic regcred --from-file=.dockerconfigjson=/home/sammy/.docker/config.json --type=kubernetes.io/dockerconfigjson
```


### Output should like this


```console
$ kubectl create secret docker-registry docker-secret1 --docker-server=docker.io --docker-username=amitkarpe --docker-password=01a52224-4ab1-473c-839c-c9a45613903e --docker-email=amitkarpe@gmail.com
secret/docker-secret created

$ kubectl get pod,secret,job
NAME                  READY   STATUS      RESTARTS   AGE
pod/fbprivate-jxkh9   0/1     Completed   0          5m42s

NAME                         TYPE                                  DATA   AGE
secret/default-token-8mksx   kubernetes.io/service-account-token   3      7h21m
secret/docker-secret         kubernetes.io/dockerconfigjson        1      6m12s

NAME                  COMPLETIONS   DURATION   AGE
job.batch/fbprivate   1/1           4s         5m42s
 
$ kubectl get secret docker-secret -o yaml 
apiVersion: v1
data:
  .dockerconfigjson: eyJhdXRocyI6eyJkb2NrZXIuaW8iOnsidXNlcm5hbWUiOiJhbWl0a2FycGUiLCJwYXNzd29yZCI6IjAxYTUyMjI0LTRhYjEtNDczYy04MzljLWM5YTQ1NjEzOTAzZSIsImVtYWlsIjoiYW1pdGthcnBlQGdtYWlsLmNvbSIsImF1dGgiOiJZVzFwZEd0aGNuQmxPakF4WVRVeU1qSTBMVFJoWWpFdE5EY3pZeTA0TXpsakxXTTVZVFExTmpFek9UQXpaUT09In19fQ==
kind: Secret
metadata:
  creationTimestamp: "2019-10-07T11:09:04Z"
  name: docker-secret
  namespace: default
  resourceVersion: "35383"
  selfLink: /api/v1/namespaces/default/secrets/docker-secret
  uid: dfe0b482-e8f2-11e9-868b-0694f6454bca
type: kubernetes.io/dockerconfigjson

$ echo "eyJhdXRocyI6eyJkb2NrZXIuaW8iOnsidXNlcm5hbWUiOiJhbWl0a2FycGUiLCJwYXNzd29yZCI6IjAxYTUyMjI0LTRhYjEtNDczYy04MzljLWM5YTQ1NjEzOTAzZSIsImVtYWlsIjoiYW1pdGthcnBlQGdtYWlsLmNvbSIsImF1dGgiOiJZVzFwZEd0aGNuQmxPakF4WVRVeU1qSTBMVFJoWWpFdE5EY3pZeTA0TXpsakxXTTVZVFExTmpFek9UQXpaUT09In19fQ=="  | base64 -d | jq
{
  "auths": {
    "docker.io": {
      "username": "amitkarpe",
      "password": "01a52224-4ab1-473c-839c-c9a45613903e",
      "email": "amitkarpe@gmail.com",
      "auth": "YW1pdGthcnBlOjAxYTUyMjI0LTRhYjEtNDczYy04MzljLWM5YTQ1NjEzOTAzZQ=="
    }
  }
}
```



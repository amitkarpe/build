# We are using prebuild image which is hosted on https://hub.docker.com/r/amitkarpe/fizzbuzz
image="amitkarpe/fizzbuzz"
tag="0.3"
max=10

all:
	make create-image
	make create-pod
	make run-pod
	make check-logs
	
create-image:
	echo "Make sure docker image is get create on same node where pod is going to run"
	make build
	make check
	make run

build: 
	docker build -t $(image):$(tag) .
	docker login -u amitkarpe -p 01a52224-4ab1-473c-839c-c9a45613903e
	docker push $(image):$(tag)

check:
	docker images | grep fizzbuzz
run:
	docker run -it $(image):$(tag) python fizzbuzz.py $(max)

create-pod:
	kubectl run fizzbuzz --image $(image):$(tag) --image-pull-policy=Always --restart=Never -o yaml --dry-run  > pod.yaml

create-deployment:
	kubectl run fizzbuzz --image $(image):$(tag) --image-pull-policy=Always --replicas=3 -o yaml --dry-run  > pod.yaml
	#kubectl delete all -l run=fizzbuzz
	kubectl get deploy,pod

run-pod:
	kubectl delete -f pod.yaml | echo "" | sleep 1 
	kubectl create -f pod.yaml && echo "success!" || echo "failure!"  
	timeout 10s kubectl get pod -w || echo ""

check-logs:
	kubectl logs fizzbuzz | less
